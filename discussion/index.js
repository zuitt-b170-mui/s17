console.log("1. Drink HTML");
console.log("2. Eat Javascript");
console.log("3. Inhale CSS");
console.log("4. Bake Bootstrap");

let tasks = ["drink html", "eat javascript", "inhale css", "bake bootstrap"];
console.log(tasks);
console.log(tasks[2]);
let indexOfLastElement = tasks.length -1;
console.log(indexOfLastElement);

// getting the number of elements
console.log(tasks.length);

// undefined
console.log(tasks[4]);

// ==========================================================
// Array Manipulation

// Adding an element
let numbers = ["one","two","three","four"];
console.log(numbers);

// using assignment operator
numbers[4] = "five";
console.log(numbers);
console.log(numbers[4]);

// Push Method
numbers.push("element");
console.log(numbers);

// callback function
function pushMethod(element){
    numbers.push(element);
};

pushMethod("six");
pushMethod("seven");
// pushMethod("eight");
console.log(numbers);

// REMOVING OF AN ELEMENT
// pop method
numbers.pop();
console.log(numbers);

function popMethod(){
    numbers.pop;
};

popMethod();
console.log(numbers);

// ============================================================
// manipulating the beginning/start of the array
// remove the first element
// shift method
numbers.shift();
console.log(numbers);

// callback function

function shiftMethod(){
    numbers.shift();
};

shiftMethod();
console.log(numbers);

// ============================================================
// ADDING AN ELEMENT
// unshift method

numbers.unshift("zero");
console.log(numbers);

// callback function

function unshiftMethod(element){
    numbers.unshift(element);
};

unshiftMethod("mcdo");
// unshiftMethod("Jollibee");
// unshiftMethod(1);
console.log(numbers);

// Arrangement of the elements

let numbs = [15, 27, 32, 12, 6, 8, 236];
console.log(numbs);

// sort method

// ascending order
numbs.sort(
    function(a,b){
        return a-b
    }
);
console.log(numbs);

// descending order
numbs.sort(
    function(a,b){
        return b-a
    }
);
console.log(numbs);

// reverse method
numbs.reverse();
console.log(numbs);


// ============================================================
// splice method (cut + paste)
// let nums = numbs.splice(1);
// let nums = numbs.splice(1,2);

// 3 parameters
let nums = numbs.splice(4,2,31,11,111);
console.log(numbs);
console.log(nums);

// ============================================================
// slice method (copy + paste)

// let slicedNums = numbs.slice(1);
let slicedNums = numbs.slice(2,7);
console.log(numbs);
console.log(slicedNums);

// ============================================================
// Merging of array
// Concat
console.log(numbers);
console.log(numbs);

let animals = ["dog", "tiger", "kangaroo", "chicken"];
console.log(animals);

let newConcat = numbers.concat(numbs, animals);
console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);

// join method
let meal = ["rice", "steak", "juice"]
console.log(meal);

newJoin = meal.join();
console.log(newJoin);

newJoin = meal.join("");
console.log(newJoin);

newJoin = meal.join(" ");
console.log(newJoin);

newJoin = meal.join("-");
console.log(newJoin);

// toString method
console.log(nums);
console.log(typeof nums);

let newString = numbs.toString();
console.log(newString);
console.log(typeof newString);


// Accessors

let countries = ["US","PH","JP","HK","SG","PH","NZ"];

// indexOf

let index = countries.indexOf("PH");
console.log(index);

index = countries.indexOf("AU");
console.log(index);

// lastIndexOf()

index = countries.lastIndexOf("PH");
console.log(index);

// returns -1 for non-existing elements
index = countries.lastIndexOf("Ph");
console.log(index);

if(countries.indexOf("PH") === -1){
    console.log("Element not existing");
}else{
    console.log("Elements exists in the array");
};

// Iterators
// forEach

let days = ["mon","tue","wed","thu","fri","sat","sun"];

// forEach
days.forEach(
    function(element){
        console.log(element)
    }
);

// map

let mapDays = days.map(
    function(element){
        return `${element} is the day of the week.`
    }
);

console.log(mapDays);
console.log(days);

// filter
console.log(numbs);
let newFilter = numbs.filter(
    function(element){
        return element < 30
    }
);

console.log(newFilter);
console.log(numbs);


// RETURNS TRUE/FALSE
// includes
let animalIncludes = animals.includes("dog");

console.log(animalIncludes);

// every
console.log(nums);

let newEvery = nums.every(
    function(element){
        return (element > 10);
    }
);

console.log(newEvery);

// some
let newSome = nums.some(
    function(element){
        return (element > 30)
    }
);

console.log(newSome);

nums.push(50);
console.log(nums);
// reduce

let newReduce = nums.reduce(
    function(a, b){
        return a + b
    }
);

console.log(newReduce);

let average = newReduce/nums.length;
console.log(average);

// toFixed

console.log(average.toFixed(2));
console.log(parseInt(average.toFixed(2)));
console.log(parseFloat(average.toFixed(2)));