let students = [];

function addStudent(element){
    students.push(element);
    console.log(element + " was added to the student's list.");
};

addStudent("John");
addStudent("Jane");
addStudent("Joe");


function countStudents(){
    console.log("There are a total of " + students.length + " students enrolled.");
};

countStudents();

function printStudents(){
    students.sort();

    students.forEach(
        function(element){
            console.log(element)
        }
    )
}

printStudents();


function findStudent(element){
    if(element === "Jane", "Joe", "John"){
        console.log(element + " is an enrollee.");
    }
    else{
        console.log("No student found with the name " + element);
    }
}

findStudent("Bill");


 